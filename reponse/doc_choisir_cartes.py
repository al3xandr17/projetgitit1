# -*- coding: utf-8 -*-
"""
Created on Mon Apr  3 14:32:17 2023

@author: Formation
"""


"""
Doit renvoyer deux cartes différentes choisi par le joueur.

param Tab : jeu de carte
type Tab : list(string)
param return : les deux cartes choisies par le joueur
type return : int

"""